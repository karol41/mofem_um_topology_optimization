/** \file topology_op.cpp

 * MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */

#include <BasicFiniteElements.hpp>

#include <cholesky.hpp>

#include <Header.hpp>

using namespace boost::numeric;
using namespace MoFEM;
using namespace std;
namespace po = boost::program_options;

static char help[] =
    "Topology optimisation module parameters: \n"
    "-my_file #path to the mesh file  \n"
    "-my_order #order of approximation \n"
    "-nb_iter #maximum number of iterations \n"
    "-young #youngs modulus (optional) \n"
    "-order_rho #order of approximation on density (optional) \n"
    "-poisson #poisson ratio \n"
    "-move # move limit for stability \n"
    "-volume_frac #constrained volume fraction \n"
    "-lambda #smoothing coefficient \n"
    "-stop_crit #stop tolerance when (change) < stop_crit (optional) \n"
    "-Lmin #minimum bisection Langrange multiplier (optional) \n"
    "-Lmax #maximum bisection Langrange multiplier (optional) \n"
    "-my_max_post_proc_ref_level 1 #level of density postprocessing (order) \n"
    "\n";

int main(int argc, char *argv[]) {

  const string default_options = "-ksp_atol 1e-10\n"
                                 "-ksp_rtol 1e-10\n"
                                 "-ksp_type fgmres\n"
                                 "-ksp_monitor\n"
                                 "-pc_type lu\n"
                                 "-pc_factor_mat_solver_package mumps\n"
                                 "-optimize 1\n"
                                 "-mat_mumps_icntl_14 800\n"
                                 "-mat_mumps_icntl_24 1\n"
                                 "-mat_mumps_icntl_13 1\n"
                                 "-mat_mumps_icntl_14 800\n"
                                 "-mat_mumps_icntl_24 1\n"
                                 "-mat_mumps_icntl_13 1\n";

  // Initialize PETSCc
  string param_file = "param_file.petsc";
   if (!static_cast<bool>(ifstream(param_file))) {
     std::ofstream file(param_file.c_str(), std::ios::ate);
     if (file.is_open()) {
       file << default_options;
       file.close();
     }
   }
 
   MoFEM::Core::Initialize(&argc, &argv, param_file.c_str(), help);
 

  try {

    PetscBool flg_block_config, flg_file;
    char mesh_file_name[255];
    char block_config_file[255];
    int nb_iter = 10;
    PetscInt order = 2;
    PetscInt rho_order = 2;
    double stop_criterion = 1e-6;
    PetscBool stop_crit_flg = PETSC_FALSE;
    PetscBool is_partitioned = PETSC_FALSE;
    PetscBool optimize_problem = PETSC_FALSE;
    PetscBool is_atom_test = PETSC_FALSE;
    double L_min = 1e-6;
    double L_max = 1e6;
    PetscInt load_cases = 0;

    enum bases { LEGENDRE, LOBATTO, BERNSTEIN_BEZIER, LASBASETOP };
    const char *list_bases[] = {"legendre", "lobatto", "bernstein_bezier"};
    PetscInt choice_base_value = LEGENDRE;

    // Read options from command line
    ierr = PetscOptionsBegin(PETSC_COMM_WORLD, "", "Config", "none");
    CHKERR(ierr);
    CHKERR PetscOptionsString("-my_file", "mesh file name", "", "mesh.h5m",
                              mesh_file_name, 255, &flg_file);

    CHKERR PetscOptionsInt("-my_order", "default approximation order", "",
                           order, &order, PETSC_NULL);
    CHKERR PetscOptionsInt("-nb_iter", "default number of iterations", "", 10,
                           &nb_iter, PETSC_NULL);

    CHKERR PetscOptionsEList("-base", "approximation base", "", list_bases,
                             LASBASETOP, list_bases[choice_base_value],
                             &choice_base_value, PETSC_NULL);

    rho_order = order;
    CHKERR PetscOptionsInt("-order_rho", "approximation order on density", "",
                           rho_order, &rho_order, PETSC_NULL);

    CHKERR PetscOptionsInt("-load_cases", "number of load cases/blocks FORCE1, FORCE2... etc", "",
                           load_cases, &load_cases, PETSC_NULL);

    CHKERR PetscOptionsScalar("-stop_crit", "stop criterion", "",
                              stop_criterion, &stop_criterion, &stop_crit_flg);
    CHKERR PetscOptionsScalar("-Lmax", "maximum langrange multiplier", "",
                              L_max, &L_max, PETSC_NULL);
    CHKERR PetscOptionsScalar("-Lmin", "minimum langrange multiplier", "",
                              L_min, &L_min, PETSC_NULL);

    CHKERR PetscOptionsBool("-is_partitioned",
                            "set if mesh is partitioned (this result that each "
                            "process keeps only part of the mes",
                            "", PETSC_FALSE, &is_partitioned, PETSC_NULL);

    CHKERR PetscOptionsBool("-optimize",
                            " "
                            "",
                            "", PETSC_FALSE, &optimize_problem, PETSC_NULL);
    CHKERR PetscOptionsBool("-is_atom_test",
                            " "
                            "",
                            "", PETSC_FALSE, &is_atom_test, PETSC_NULL);

    ierr = PetscOptionsEnd();
    CHKERRG(ierr);

    // Throw error if file with mesh is not give
    if (flg_file != PETSC_TRUE) {
      SETERRQ(PETSC_COMM_SELF, 1, "*** ERROR -my_file (MESH FILE NEEDED)");
    }

    // Create mesh database
    moab::Core mb_instance;
    moab::Interface &moab = mb_instance;

    // Create moab communicator
    // Create separate MOAB communicator, it is duplicate of PETSc communicator.
    // NOTE That this should eliminate potential communication problems between
    // MOAB and PETSC functions.
    MPI_Comm moab_comm_world;
    MPI_Comm_dup(PETSC_COMM_WORLD, &moab_comm_world);
    ParallelComm *pcomm = ParallelComm::get_pcomm(&moab, MYPCOMM_INDEX);
    if (pcomm == NULL)
      pcomm = new ParallelComm(&moab, moab_comm_world);

    // Read whole mesh or part of is if partitioned
    if (is_partitioned == PETSC_TRUE) {
      // This is a case of distributed mesh and algebra. In that case each
      // processor
      // keep only part of the problem.
      const char *option;
      option = "PARALLEL=READ_PART;"
               "PARALLEL_RESOLVE_SHARED_ENTS;"
               "PARTITION=PARALLEL_PARTITION;";
      CHKERR moab.load_file(mesh_file_name, 0, option);
    } else {
      int rank;
      MPI_Comm_rank(PETSC_COMM_WORLD, &rank);
      if (rank != 0) {
        SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY,
                "Mesh has to be partitioned for parallel calculations. Use "
                "mofem_part tool and parameter: "
                "-is_partitioned.");
      }
      const char *option;
      option = "";
      CHKERR moab.load_file(mesh_file_name, 0, option);
    }

    // Create MoFEM database and link it to MoAB
    MoFEM::Core core(moab);
    MoFEM::Interface &m_field = core;

    // Print boundary conditions and material parameters
    MeshsetsManager *meshsets_mng_ptr;
    CHKERR m_field.getInterface(meshsets_mng_ptr);
    CHKERR meshsets_mng_ptr->printDisplacementSet();
    CHKERR meshsets_mng_ptr->printForceSet();
    CHKERR meshsets_mng_ptr->printMaterialsSet();

    // Set bit refinement level to all entities (we do not refine mesh in this
    // example
    // so all entities have the same bit refinement level)
    BitRefLevel bit_level0;
    bit_level0.set(0);
    CHKERR m_field.getInterface<BitRefManager>()->setBitRefLevelByDim(
        0, 3, bit_level0);
    EntityHandle root_set = moab.get_root_set();
    const int verb = m_field.get_comm_rank() == 0 ? -1 : -1; // FIXME:

    // **** ADD FIELDS **** //
    // Declare approximation fields
    FieldApproximationBase base = NOBASE;
    switch (choice_base_value) {
    case LEGENDRE:
      base = AINSWORTH_LEGENDRE_BASE;
      break;
    case LOBATTO:
      base = AINSWORTH_LOBATTO_BASE;
      break;
    case BERNSTEIN_BEZIER:
      base = AINSWORTH_BERNSTEIN_BEZIER_BASE;
      break;
    default:
      SETERRQ(PETSC_COMM_WORLD, MOFEM_NOT_IMPLEMENTED, "Base not implemented");
    };

    // add fields
    bool add_rho_field = false;
    if (!m_field.check_field("RHO")) {
      CHKERR m_field.add_field("RHO", H1, base, 1, MB_TAG_SPARSE, MF_ZERO,
                               verb);
      CHKERR m_field.add_ents_to_field_by_type(root_set, MBTET, "RHO");

      // set approx orders
      if (base == AINSWORTH_BERNSTEIN_BEZIER_BASE)
        CHKERR m_field.set_field_order(root_set, MBVERTEX, "RHO", rho_order);
      else
        CHKERR m_field.set_field_order(root_set, MBVERTEX, "RHO", 1);
      CHKERR m_field.set_field_order(root_set, MBEDGE, "RHO", rho_order);
      CHKERR m_field.set_field_order(root_set, MBTRI, "RHO", rho_order);
      CHKERR m_field.set_field_order(root_set, MBTET, "RHO", rho_order);
      add_rho_field = true;
    }
    CHKERR m_field.add_field("MESH_NODE_POSITIONS", H1, AINSWORTH_LEGENDRE_BASE,
                             3);

    CHKERR m_field.add_ents_to_field_by_type(root_set, MBTET,
                                             "MESH_NODE_POSITIONS");

    CHKERR m_field.add_field("DISPLACEMENTS", H1, base, 3, MB_TAG_SPARSE,
                             MF_ZERO, verb);

    CHKERR m_field.add_ents_to_field_by_type(root_set, MBTET, "DISPLACEMENTS");
    if (base == AINSWORTH_BERNSTEIN_BEZIER_BASE)
      CHKERR m_field.set_field_order(root_set, MBVERTEX, "DISPLACEMENTS",
                                     order);
    else
      CHKERR m_field.set_field_order(root_set, MBVERTEX, "DISPLACEMENTS", 1);
    CHKERR m_field.set_field_order(root_set, MBEDGE, "DISPLACEMENTS", order);
    CHKERR m_field.set_field_order(root_set, MBTRI, "DISPLACEMENTS", order);
    CHKERR m_field.set_field_order(root_set, MBTET, "DISPLACEMENTS", order);

    CHKERR m_field.set_field_order(root_set, MBVERTEX, "MESH_NODE_POSITIONS",
                                   1);

    CHKERR m_field.build_fields(verb);

    {
      Projection10NodeCoordsOnField ent_method_material(m_field,
                                                        "MESH_NODE_POSITIONS");
      CHKERR m_field.loop_dofs("MESH_NODE_POSITIONS", ent_method_material);
    }
    // **** ADD MOMENTUM FLUXES **** //
    // setup elements for loading
    CHKERR MetaNeummanForces::addNeumannBCElements(m_field, "DISPLACEMENTS");
    CHKERR MetaNodalForces::addElement(m_field, "DISPLACEMENTS");
    CHKERR MetaEdgeForces::addElement(m_field, "DISPLACEMENTS");

    // **** ADD ELEMENTS **** //
    // Add finite element (this defines element declaration comes later)
    CHKERR m_field.add_finite_element("ELASTIC");
    CHKERR m_field.modify_finite_element_add_field_row("ELASTIC",
                                                       "DISPLACEMENTS");
    CHKERR m_field.modify_finite_element_add_field_col("ELASTIC",
                                                       "DISPLACEMENTS");
    CHKERR m_field.modify_finite_element_add_field_data("ELASTIC",
                                                        "DISPLACEMENTS");
    CHKERR m_field.modify_finite_element_add_field_data("ELASTIC", "RHO");
    CHKERR m_field.modify_finite_element_add_field_data("ELASTIC",
                                                        "MESH_NODE_POSITIONS");
    CHKERR m_field.add_ents_to_finite_element_by_type(root_set, MBTET,
                                                      "ELASTIC");

    CHKERR m_field.add_finite_element("RHO_APPROX");
    CHKERR m_field.modify_finite_element_add_field_row("RHO_APPROX", "RHO");
    CHKERR m_field.modify_finite_element_add_field_col("RHO_APPROX", "RHO");
    CHKERR m_field.modify_finite_element_add_field_data("RHO_APPROX", "RHO");
    CHKERR m_field.modify_finite_element_add_field_data("RHO_APPROX",
                                                        "DISPLACEMENTS");
    CHKERR m_field.modify_finite_element_add_field_data("RHO_APPROX",
                                                        "MESH_NODE_POSITIONS");
    CHKERR m_field.add_ents_to_finite_element_by_type(root_set, MBTET,
                                                      "RHO_APPROX");

    // build finite elements
    CHKERR m_field.build_finite_elements(verb);
    // build adjacencies between elements and degrees of freedom
    CHKERR m_field.build_adjacencies(bit_level0);

    CommonData commonData(m_field);
    commonData.getParameters();

    double init_density = commonData.volumeFrac;
    if (add_rho_field)
      CHKERR m_field.getInterface<FieldBlas>()->setField(init_density, MBVERTEX,
                                                         "RHO");
    // set integration rule
    // auto VolRule = [](int r, int c, int d) { return 2 * d + 0; };
    auto VolRule = [](int r, int c, int d) { return 2 * d + 0; };

    auto print_field = [&](const string field_name) { // for debugging
      MoFEMFunctionBeginHot;
      cerr << "printing field start:" << endl;
      for (_IT_GET_DOFS_FIELD_BY_NAME_FOR_LOOP_(m_field, field_name.c_str(),
                                                dof)) {
        if ((*dof)->getEntType() == MBVERTEX)
          cerr << (*dof)->getFieldData() << endl;
      }
      cerr << "printing field end:" << endl;
      cerr << "printing HO field start:" << endl;
      for (_IT_GET_DOFS_FIELD_BY_NAME_FOR_LOOP_(m_field, field_name.c_str(),
                                                dof)) {
        if ((*dof)->getEntType() != MBVERTEX)
          cerr << (*dof)->getFieldData() << endl;
      }
      cerr << "printing HO field end:" << endl;
      MoFEMFunctionReturnHot(0);
    };

    boost::shared_ptr<VolumeElementForcesAndSourcesCore> elastic_fe(
        new VolumeElementForcesAndSourcesCore(m_field));
    boost::shared_ptr<VolumeElementForcesAndSourcesCore> calc_sens(
        new VolumeElementForcesAndSourcesCore(m_field));

    elastic_fe->getRuleHook = VolRule;
    calc_sens->getRuleHook = VolRule;
    elastic_fe->getOpPtrVector().push_back(
        new OpCalculateScalarFieldValues("RHO", commonData.dataRhotGaussPtr));
    elastic_fe->getOpPtrVector().push_back(new OpAssembleK(commonData));

    calc_sens->getOpPtrVector().push_back(
        new OpCalculateScalarFieldValues("RHO", commonData.dataRhotGaussPtr));
    calc_sens->getOpPtrVector().push_back(
        new OpCalculateVectorFieldGradient<3, 3>("DISPLACEMENTS",
                                                 commonData.dispGradAtGaussPts));
    calc_sens->getOpPtrVector().push_back(
        new OpCalculateSensitivities(commonData));

    // **** BUILD DM **** //
    DM dm;
    DM sub_dm_elastic;
    DM sub_dm_rho;

    CHKERR commonData.build_main_dm(dm, sub_dm_elastic, sub_dm_rho, m_field,
                                    bit_level0, is_partitioned);

    // search for entities to preserve and delete them from approx problem
    // cerr << "deleting dofs" << endl;
    // Range ents_from_blocks;
    // {
    //   auto set_densities_from_blocks = [&]() {
    //     MoFEMFunctionBeginHot;
    //     for (_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(m_field, BLOCKSET, it))
    //     {
    //       string name = it->getName();
    //       if (name.compare(0, 7, "DENSITY") == 0) {
    //         cerr << "my block name is: " << name << endl;
    //         for (int dim = 0; dim < 3; dim++) {
    //           Range ents;
    //           CHKERR it->getMeshsetIdEntitiesByDimension(m_field.get_moab(),
    //                                                      dim, ents, true);
    //           if (dim > 1) {
    //             Range _edges;
    //             CHKERR m_field.get_moab().get_adjacencies(
    //                 ents, 1, false, _edges, moab::Interface::UNION);
    //             ents.insert(_edges.begin(), _edges.end());
    //           }
    //           if (dim > 0) {
    //             Range _nodes;
    //             CHKERR m_field.get_moab().get_connectivity(ents, _nodes,
    //             true); ents.insert(_nodes.begin(), _nodes.end());
    //           }
    //           ents_from_blocks.merge(ents);
    //           const Problem *problem_ptr;
    //           CHKERR m_field.get_problem("DM_TOPO", &problem_ptr);
    //           for (Range::iterator eit = ents.begin(); eit != ents.end();
    //                eit++) {
    //             for (_IT_NUMEREDDOF_ROW_BY_NAME_ENT_PART_FOR_LOOP_(
    //                      problem_ptr, "RHO", *eit, pcomm->rank(), dof_ptr)) {
    //               NumeredDofEntity *dof = dof_ptr->get();
    //               if (dof->getEntType() == MBVERTEX) {
    //                 (*dof_ptr)->getFieldData() = 1;
    //               } else {
    //                 (*dof_ptr)->getFieldData() = 0;
    //               }
    //             }
    //           }
    //         }
    //         // remove dofs on density problem
    //       }
    //     }
    //     MoFEMFunctionReturnHot(0);
    //   };
    //   // cerr << "ents_from_blocks on proc " << m_field.get_comm_rank() << "
    //   : "
    //   // << ents_from_blocks << endl;

    //   CHKERR set_densities_from_blocks();
    //   CHKERR m_field.synchronise_entities(ents_from_blocks);
    //     cerr << "ents_from_blocks on proc " << m_field.get_comm_rank() << " :
    //     " << ents_from_blocks.size() << endl;
    //   CHKERR m_field.getInterface<ProblemsManager>()->removeDofsOnEntities(
    //       "DM_TOPO", "RHO", ents_from_blocks);
    // }
    // CHKERR PetscBarrier(PETSC_NULL);
    CHKERR commonData.build_sub_dms(dm, sub_dm_elastic, sub_dm_rho, m_field,
                                    bit_level0, is_partitioned);
    Mat A;
    Vec x, f;

    CHKERR DMCreateMatrix(sub_dm_elastic, &A);
    CHKERR DMCreateGlobalVector(sub_dm_elastic, &x);
    CHKERR VecZeroEntries(x);
    CHKERR VecDuplicate(x, &f);
    CHKERR VecGhostUpdateBegin(x, INSERT_VALUES, SCATTER_FORWARD);
    CHKERR VecGhostUpdateEnd(x, INSERT_VALUES, SCATTER_FORWARD);
    CHKERR DMoFEMMeshToLocalVector(sub_dm_elastic, x, INSERT_VALUES,
                                   SCATTER_REVERSE);
    CHKERR MatZeroEntries(A);

    Mat K;
    Vec d, d0, r_vec;

    CHKERR DMCreateMatrix(sub_dm_rho, &K);
    CHKERR DMCreateGlobalVector(sub_dm_rho, &d);
    CHKERR VecZeroEntries(d);
    CHKERR VecDuplicate(d, &d0);
    CHKERR VecDuplicate(d, &r_vec);
    CHKERR MatZeroEntries(K);

    boost::shared_ptr<VolumeElementForcesAndSourcesCore> rho_approximation_RHS(
        new VolumeElementForcesAndSourcesCore(m_field));
    boost::shared_ptr<VolumeElementForcesAndSourcesCore> rho_approximation_LHS(
        new VolumeElementForcesAndSourcesCore(m_field));

    rho_approximation_RHS->getRuleHook = VolRule;
    rho_approximation_LHS->getRuleHook = VolRule;

    rho_approximation_LHS->ksp_B = K;
    rho_approximation_RHS->ksp_f = r_vec;
    rho_approximation_LHS->getOpPtrVector().push_back(
        new OpAssembleRhoLhs("RHO", "RHO", commonData));

    // rho_approximation_RHS->getOpPtrVector().push_back(
    //     new OpCalculateScalarFieldValues("RHO",
    //     commonData.dataRhotGaussPtr));
    rho_approximation_RHS->getOpPtrVector().push_back(
        new OpUpdateDensity<false>(commonData));
    rho_approximation_RHS->getOpPtrVector().push_back(
        new OpAssembleDensityRhs("RHO", commonData));

    boost::shared_ptr<FEMethod> density_bc_ptr;
    density_bc_ptr = boost::shared_ptr<FEMethod>(
        new DirichletDensityBc(m_field, "RHO", K, d0, r_vec));
    density_bc_ptr->snes_ctx = FEMethod::CTX_SNESNONE;
    density_bc_ptr->ts_ctx = FEMethod::CTX_TSNONE;

    // **** SET VARIABLES FOR OPTIMIZATION **** //
    Vec volume_vec;
    Vec length_vec;
    int vec_ghost[] = {0};
    CHKERR VecCreateGhost(PETSC_COMM_WORLD, (!m_field.get_comm_rank()) ? 1 : 0,
                          1, 1, vec_ghost, &volume_vec);

    CHKERR VecZeroEntries(volume_vec);
    VecDuplicate(volume_vec, &commonData.massVec);
    VecDuplicate(volume_vec, &commonData.changeVec);
    CHKERR VecCreateMPI(m_field.get_comm(), 1, PETSC_DECIDE, &length_vec);

    auto get_edge_length = [&]() {
      MoFEMFunctionBeginHot;
      Range tets, edges;

      CHKERR moab.get_entities_by_dimension(0, 3, tets);
      CHKERR moab.get_adjacencies(tets, 1, true, edges, moab::Interface::UNION);
      // Store edge nodes coordinates in FTensor
      double edge_node_coords[6];
      FTensor::Tensor1<double *, 3> t_node_edge[2] = {
          FTensor::Tensor1<double *, 3>(edge_node_coords, &edge_node_coords[1],
                                        &edge_node_coords[2]),
          FTensor::Tensor1<double *, 3>(&edge_node_coords[3],
                                        &edge_node_coords[4],
                                        &edge_node_coords[5])};
      FTensor::Index<'i', 3> i;
      double max_edge_l = 0;
      for (auto edge : edges) {
        int num_nodes;
        const EntityHandle *conn;
        CHKERR moab.get_connectivity(edge, conn, num_nodes, true);
        CHKERR moab.get_coords(conn, num_nodes, edge_node_coords);
        t_node_edge[0](i) -= t_node_edge[1](i);
        double l = sqrt(t_node_edge[0](i) * t_node_edge[0](i));
        max_edge_l = (max_edge_l < l) ? l : max_edge_l;
      }
      CHKERR VecSetValue(length_vec, m_field.get_comm_rank(), max_edge_l, INSERT_VALUES);
      MoFEMFunctionReturnHot(0);
    };

    CHKERR get_edge_length();
    CHKERR VecAssemblyBegin(length_vec);
    CHKERR VecAssemblyEnd(length_vec);
    CHKERR VecMax(length_vec, PETSC_NULL, &commonData.edgeLength);

    CHKERR PetscPrintf(PETSC_COMM_WORLD, "Max edge length: %3.3g. \n",
                       commonData.edgeLength);

    // set the initial field values properly
    {
      //*** set field values to the vector d
      // CHKERR VecGhostUpdateBegin(d0, INSERT_VALUES, SCATTER_FORWARD);
      // CHKERR VecGhostUpdateEnd(d0, INSERT_VALUES, SCATTER_FORWARD);

      CHKERR DMoFEMMeshToLocalVector(sub_dm_rho, d0, INSERT_VALUES,
                                     SCATTER_FORWARD);
      CHKERR VecGhostUpdateBegin(d0, INSERT_VALUES, SCATTER_FORWARD);
      CHKERR VecGhostUpdateEnd(d0, INSERT_VALUES, SCATTER_FORWARD);

      CHKERR DMoFEMPreProcessFiniteElements(sub_dm_rho, density_bc_ptr.get());
      // CHKERR VecGhostUpdateBegin(d0, ADD_VALUES, SCATTER_REVERSE);
      // CHKERR VecGhostUpdateEnd(d0, ADD_VALUES, SCATTER_REVERSE);

      CHKERR VecGhostUpdateBegin(d0, INSERT_VALUES, SCATTER_FORWARD);
      CHKERR VecGhostUpdateEnd(d0, INSERT_VALUES, SCATTER_FORWARD);
      CHKERR DMoFEMMeshToLocalVector(sub_dm_rho, d0, INSERT_VALUES,
                                     SCATTER_REVERSE);
    }
    // keep only BC values on d0 vector
    CHKERR VecZeroEntries(d0);
    CHKERR VecGhostUpdateBegin(d0, INSERT_VALUES, SCATTER_FORWARD);
    CHKERR VecGhostUpdateEnd(d0, INSERT_VALUES, SCATTER_FORWARD);
    CHKERR DMoFEMPreProcessFiniteElements(sub_dm_rho, density_bc_ptr.get());
    CHKERR VecGhostUpdateBegin(d0, ADD_VALUES, SCATTER_REVERSE);
    CHKERR VecGhostUpdateEnd(d0, ADD_VALUES, SCATTER_REVERSE);
    // CHKERR VecGhostUpdateBegin(d0, INSERT_VALUES, SCATTER_FORWARD);
    // CHKERR VecGhostUpdateEnd(d0, INSERT_VALUES, SCATTER_FORWARD);
    CHKERR DMoFEMLoopFiniteElements(sub_dm_rho, "RHO_APPROX",
                                    rho_approximation_LHS);

    CHKERR DMoFEMPostProcessFiniteElements(sub_dm_rho, density_bc_ptr.get());
    density_bc_ptr->snes_B = PETSC_NULL; // NOTE: avoid reassembling

    // **** BOUNDARY CONDITIONS **** //
    boost::shared_ptr<FEMethod> nullFE;

    elastic_fe->ksp_B = A;
    elastic_fe->ksp_f = f;

      bool flag_cubit_disp = false;
    for (_IT_CUBITMESHSETS_BY_BCDATA_TYPE_FOR_LOOP_(
             m_field, NODESET | DISPLACEMENTSET, it)) {
      flag_cubit_disp = true;
    }

    boost::shared_ptr<FEMethod> dirichlet_bc_ptr;
    if (!flag_cubit_disp) {
      dirichlet_bc_ptr =
          boost::shared_ptr<FEMethod>(new DirichletSetFieldFromBlockWithFlags(
              m_field, "DISPLACEMENTS", "DISPLACEMENT", A, x, f));
    } else {
      dirichlet_bc_ptr = boost::shared_ptr<FEMethod>(
          new DirichletDisplacementBc(m_field, "DISPLACEMENTS", A, x, f));
    }

    dirichlet_bc_ptr->snes_ctx = FEMethod::CTX_SNESNONE;
    dirichlet_bc_ptr->ts_ctx = FEMethod::CTX_TSNONE;

    CHKERR DMoFEMPreProcessFiniteElements(sub_dm_elastic,
                                          dirichlet_bc_ptr.get());

    CHKERR DMoFEMLoopFiniteElements(sub_dm_elastic, "ELASTIC", elastic_fe);
    // Assemble pressure and traction forces. Run particular
    // implemented for do this, see MetaNeummanForces how this is
    // implemented.
    boost::ptr_map<std::string, NeummanForcesSurface> neumann_forces;
    CHKERR MetaNeummanForces::setMomentumFluxOperators(m_field, neumann_forces,
                                                       f, "DISPLACEMENTS");
    {
      boost::ptr_map<std::string, NeummanForcesSurface>::iterator mit =
          neumann_forces.begin();
      for (; mit != neumann_forces.end(); mit++) {
        CHKERR DMoFEMLoopFiniteElements(sub_dm_elastic, mit->first.c_str(),
                                        &mit->second->getLoopFe());
      }
    }
    // Assemble forces applied to nodes, see implementation in NodalForce
    boost::ptr_map<std::string, NodalForce> nodal_forces;
    CHKERR MetaNodalForces::setOperators(m_field, nodal_forces, f,
                                         "DISPLACEMENTS");
    {
      boost::ptr_map<std::string, NodalForce>::iterator fit =
          nodal_forces.begin();
      for (; fit != nodal_forces.end(); fit++) {
        CHKERR DMoFEMLoopFiniteElements(sub_dm_elastic, fit->first.c_str(),
                                        &fit->second->getLoopFe());
      }
    }
    // Assemble edge forces
    boost::ptr_map<std::string, EdgeForce> edge_forces;
    CHKERR MetaEdgeForces::setOperators(m_field, edge_forces, f,
                                        "DISPLACEMENTS");
    {
      boost::ptr_map<std::string, EdgeForce>::iterator fit =
          edge_forces.begin();
      for (; fit != edge_forces.end(); fit++) {
        CHKERR DMoFEMLoopFiniteElements(sub_dm_elastic, fit->first.c_str(),
                                        &fit->second->getLoopFe());
      }
    }
    // Assemble pres
    CHKERR DMoFEMPostProcessFiniteElements(sub_dm_elastic,
                                           dirichlet_bc_ptr.get());

    boost::shared_ptr<VolumeElementForcesAndSourcesCore> init_mass_el(
        new VolumeElementForcesAndSourcesCore(m_field));
    init_mass_el->getOpPtrVector().push_back(
        new OpVolumeCalculation(volume_vec));
    CHKERR DMoFEMLoopFiniteElements(sub_dm_elastic, "ELASTIC", init_mass_el);
    init_mass_el->getRuleHook = VolRule;

    double initial_vol;
    CHKERR VecAssemblyBegin(volume_vec);
    CHKERR VecAssemblyEnd(volume_vec);
    CHKERR VecSum(volume_vec, &initial_vol);
    // initial_vol *= init_density;
    boost::shared_ptr<VolumeElementForcesAndSourcesCore> up_density(
        new VolumeElementForcesAndSourcesCore(m_field));

    // up_density->getOpPtrVector().push_back(
    //     new OpCalculateScalarFieldValues("RHO",
    //     commonData.dataRhotGaussPtr));
    up_density->getOpPtrVector().push_back(
        new OpUpdateDensity<true>(commonData));
    up_density->getRuleHook = VolRule;

    boost::shared_ptr<VolumeElementForcesAndSourcesCore> calc_mass(
        new VolumeElementForcesAndSourcesCore(m_field));
    // calc_mass->getOpPtrVector().push_back(
    //     new OpCalculateScalarFieldValues("RHO",
    //     commonData.dataRhotGaussPtr));
    calc_mass->getOpPtrVector().push_back(
        new OpUpdateDensity<false>(commonData));
    Vec mass_vec;
    calc_mass->getOpPtrVector().push_back(
        new OpMassCalculation("RHO", commonData, mass_vec));
    calc_mass->getRuleHook = VolRule;

    // Set up post-procesor. It is some generic implementation of finite
    // element.
    PostProcVolumeOnRefinedMesh post_proc(m_field);
    CHKERR post_proc.generateReferenceElementMesh();
    CHKERR post_proc.addFieldValuesPostProc("DISPLACEMENTS");
    CHKERR post_proc.addFieldValuesPostProc("RHO");
    CHKERR post_proc.addFieldValuesGradientPostProc("DISPLACEMENTS");
    post_proc.getOpPtrVector().push_back(
        new OpCalculateVectorFieldGradient<3, 3>("DISPLACEMENTS",
                                                 commonData.dispGradAtGaussPts));
    post_proc.getOpPtrVector().push_back(new OpPostProcCompliance(
        post_proc.postProcMesh, post_proc.mapGaussPts, commonData));

    // **** SOLVE **** //

    // this vector never changes
    CHKERR VecGhostUpdateBegin(f, ADD_VALUES, SCATTER_REVERSE);
    CHKERR VecGhostUpdateEnd(f, ADD_VALUES, SCATTER_REVERSE);
    CHKERR VecAssemblyBegin(f);
    CHKERR VecAssemblyEnd(f);
    dirichlet_bc_ptr->ksp_f = PETSC_NULL; // NOTE: trick to avoid reassembling

    KSP solver0, solver1;
    CHKERR KSPCreate(PETSC_COMM_WORLD, &solver0);
    // CHKERR KSPAppendOptionsPrefix(solver0, "elastic_");
    CHKERR KSPSetFromOptions(solver0);
    CHKERR KSPSetOperators(solver0, A, A);
    CHKERR KSPSetUp(solver0);

    CHKERR KSPCreate(PETSC_COMM_WORLD, &solver1);
    // CHKERR KSPAppendOptionsPrefix(solver1, "density_");
    CHKERR KSPSetFromOptions(solver1);
    // CHKERR KSPMonitorCancel(solver1);
    CHKERR KSPSetOperators(solver1, K, K);
    CHKERR KSPSetUp(solver1);

    for (unsigned int j = 0; j != nb_iter; ++j) {
      CHKERR PetscPrintf(PETSC_COMM_WORLD, "Solve elastic problem iter: %d. \n",
                         j);
      CHKERR KSPSolve(solver0, f, x);

      // save solution in vector x on mesh
      CHKERR VecGhostUpdateBegin(x, INSERT_VALUES, SCATTER_FORWARD);
      CHKERR VecGhostUpdateEnd(x, INSERT_VALUES, SCATTER_FORWARD);
      CHKERR DMoFEMMeshToGlobalVector(sub_dm_elastic, x, INSERT_VALUES,
                                      SCATTER_REVERSE);

      double sum_of_changes = 0.;
      if (optimize_problem) {

        // ** OPTIMIZE ** //
        CHKERR DMoFEMLoopFiniteElements(sub_dm_rho, "RHO_APPROX", calc_sens);

        auto approximate_density = [&]() {
          MoFEMFunctionBegin;
          CHKERR VecZeroEntries(r_vec);
          CHKERR VecGhostUpdateBegin(r_vec, INSERT_VALUES, SCATTER_FORWARD);
          CHKERR VecGhostUpdateEnd(r_vec, INSERT_VALUES, SCATTER_FORWARD);
          // //*** set field values to the vector d
          // CHKERR DMoFEMMeshToLocalVector(sub_dm_rho, d, INSERT_VALUES,
          //                                SCATTER_FORWARD);
          // CHKERR VecGhostUpdateBegin(d, INSERT_VALUES, SCATTER_FORWARD);
          // CHKERR VecGhostUpdateEnd(d, INSERT_VALUES, SCATTER_FORWARD);

          // cerr << "PRINT VECTOR d" << endl;
          // VecView(d0, PETSC_VIEWER_STDOUT_WORLD);
          // CHKERR print_field("RHO");
          // Assemble pres
          // CHKERR DMoFEMPreProcessFiniteElements(sub_dm_rho,
          //                                       density_bc_ptr.get());
          //                                       //modify d
          // CHKERR VecGhostUpdateBegin(d, INSERT_VALUES, SCATTER_FORWARD);
          // CHKERR VecGhostUpdateEnd(d, INSERT_VALUES, SCATTER_FORWARD);

          //*** set the vector values to the field
          // CHKERR DMoFEMMeshToLocalVector(sub_dm_rho, d, INSERT_VALUES,
          // SCATTER_REVERSE); cout << "AFTER APPLYING BC" << endl; CHKERR
          // print_field("RHO");

          CHKERR DMoFEMLoopFiniteElements(sub_dm_rho, "RHO_APPROX",
                                          rho_approximation_RHS);
          // cerr << "PRINT VECTOR rhs vector" << endl;
          // VecView(r_vec, PETSC_VIEWER_STDOUT_WORLD);
          CHKERR VecAssemblyBegin(r_vec);
          CHKERR VecAssemblyEnd(r_vec);
          CHKERR VecGhostUpdateBegin(r_vec, ADD_VALUES, SCATTER_REVERSE);
          CHKERR VecGhostUpdateEnd(r_vec, ADD_VALUES, SCATTER_REVERSE);

          // CHKERR DMoFEMPreProcessFiniteElements(sub_dm_rho,
          //                                       density_bc_ptr.get()); //
          //                                       bc vals on d0

          // CHKERR VecGhostUpdateBegin(d0, INSERT_VALUES, SCATTER_FORWARD);
          // CHKERR VecGhostUpdateEnd(d0, INSERT_VALUES, SCATTER_FORWARD);

          // cerr << "PRINT VECTOR d0 vector" << endl;
          // VecView(d0, PETSC_VIEWER_STDOUT_WORLD);

          CHKERR DMoFEMPostProcessFiniteElements(
              sub_dm_rho,
              density_bc_ptr.get()); // sets zeros to r_vec
          // CHKERR print_field("RHO");
          // cerr << "PRINT VECTOR rhs vector" << endl;
          // VecView(r_vec, PETSC_VIEWER_STDOUT_WORLD);
          CHKERR VecGhostUpdateBegin(r_vec, ADD_VALUES, SCATTER_REVERSE);
          CHKERR VecGhostUpdateEnd(r_vec, ADD_VALUES, SCATTER_REVERSE);
          CHKERR VecAssemblyBegin(r_vec); // NOTE: previous command does it
                                          // too (postprocessfinite)
          CHKERR VecAssemblyEnd(r_vec);

          // cerr << "PRINT VECTOR r_vec" << endl;
          // VecView(r_vec, PETSC_VIEWER_STDOUT_WORLD);

          //  cerr << "PRINT VECTOR d0" << endl;
          // VecView(d0, PETSC_VIEWER_STDOUT_WORLD);

          CHKERR KSPSolve(solver1, r_vec, d);
          // cerr << "PRINT VECTOR d" << endl;
          // VecView(d, PETSC_VIEWER_STDOUT_WORLD);
          CHKERR VecAXPY(d, 1., d0);
          // cerr << "PRINT VECTOR d after AXPY" << endl;
          // VecView(d, PETSC_VIEWER_STDOUT_WORLD);

          // cerr << "PRINT VECTOR d0" << endl;
          // VecView(d0, PETSC_VIEWER_STDOUT_WORLD);
          CHKERR VecGhostUpdateBegin(d, INSERT_VALUES, SCATTER_FORWARD);
          CHKERR VecGhostUpdateEnd(d, INSERT_VALUES, SCATTER_FORWARD);
          //*** set the vector values to the field
          CHKERR DMoFEMMeshToGlobalVector(sub_dm_rho, d, INSERT_VALUES,
                                          SCATTER_REVERSE);

          //  CHKERR print_field("RHO");
          for (_IT_GET_DOFS_FIELD_BY_NAME_FOR_LOOP_(m_field, "RHO",
                                                    dof)) { // for testing
            double &data = (*dof)->getFieldData();
            if ((*dof)->getEntType() == MBVERTEX)
              data = data > 1.0 ? 1. : (data < 0.001 ? 0.001 : data);
            // else
            //   data = 0; // this is for post processing
          }
          // this will truncate all the dofs (even HO)
          //  auto Axpy = [](double &fy, double fx) {
          //    MoFEMFunctionBeginHot;
          //    fy = fx > 1.0 ? 1. : (fx < 0.001 ? 0.001 : fx);
          //    MoFEMFunctionReturnHot(0);
          //  };
          //  CHKERR m_field.getInterface<FieldBlas>()->fieldLambda(Axpy,
          //  "RHO",
          //                                                        "RHO");
          CHKERR m_field.synchronise_field_entities("RHO", 0);
          // CHKERR print_field("RHO");
          MoFEMFunctionReturn(0);
        };

        auto calculate_mass = [&]() {
          MoFEMFunctionBegin;
          CHKERR DMoFEMLoopFiniteElements(sub_dm_rho, "RHO_APPROX", calc_mass);

          CHKERR VecAssemblyBegin(commonData.massVec);
          CHKERR VecAssemblyEnd(commonData.massVec);
          CHKERR VecSum(commonData.massVec, &commonData.cUrrent_mass);

          CHKERR VecZeroEntries(commonData.massVec);

          MoFEMFunctionReturn(0);
        };

        double L1 = 0;
        double L2 = L_max;
        double &L_mid = commonData.L_mid;
        L_mid = 0;
        int iterator = 0;
        while (L2 - L1 > L_min) { // TODO: find something better than bisection
          const double difference = L2 - L1;
          L_mid = 0.5 * (L2 + L1);
          commonData.cUrrent_mass = 0;

          CHKERR calculate_mass();

          if (commonData.cUrrent_mass - initial_vol * commonData.volumeFrac >
              0) {
            L1 = L_mid;
          } else {
            L2 = L_mid;
          }

          if (m_field.get_comm_rank() == 0) {
            CHKERR PetscPrintf(PETSC_COMM_WORLD,
                               "     Step: %d. Lmid: %1.1e. L2-L1: %1.1e. "
                               "Mass: %3.4g Vol_frac: %1.4g.\n",
                               iterator++, commonData.L_mid, difference,
                               commonData.cUrrent_mass,
                               commonData.cUrrent_mass / initial_vol);
          }

          if (is_atom_test) {
            if (j == 1) { // global iteration
              if (iterator == 28) {
                if (fabs(commonData.cUrrent_mass - 3.4944) > 1e-4) {
                  SETERRQ(PETSC_COMM_SELF, MOFEM_ATOM_TEST_INVALID,
                          "atom test diverged!");
                }
              }
            }
          }
        }
        CHKERR PetscPrintf(PETSC_COMM_WORLD,
                           "  Density field approximation. \n");
        CHKERR approximate_density();
        CHKERR calculate_mass();
        if (m_field.get_comm_rank() == 0) {
          CHKERR PetscPrintf(PETSC_COMM_WORLD,
                             "     Mass after approximation: %3.6g. \n",
                             commonData.cUrrent_mass);
        }


        if (stop_crit_flg) {
          CHKERR DMoFEMLoopFiniteElements(sub_dm_rho, "RHO_APPROX", up_density);
          CHKERR VecAssemblyBegin(commonData.changeVec);
          CHKERR VecAssemblyEnd(commonData.changeVec);
          CHKERR VecSum(commonData.changeVec, &sum_of_changes);
          CHKERR VecZeroEntries(commonData.changeVec);
          sum_of_changes = sum_of_changes / initial_vol;
        }
      }

      CHKERR DMoFEMLoopFiniteElements(sub_dm_rho, "RHO_APPROX", &post_proc);
      ostringstream file;
      file << "out_" << j << ".h5m";
      CHKERR PetscPrintf(PETSC_COMM_WORLD,
                         "Saved iteration number %d Stop criterion: %3.5g \n",
                         j, sum_of_changes);

      CHKERR post_proc.writeFile(file.str().c_str());

      if (!optimize_problem ||
          (sum_of_changes < stop_criterion && stop_crit_flg)) {
        CHKERR PetscPrintf(PETSC_COMM_WORLD,
                           "Stop criterion fullfilled: %3.3g < %3.3g \n",
                           sum_of_changes, stop_criterion);
        break;
      }

      CHKERR MatZeroEntries(A);
      // CHKERR DMoFEMPreProcessFiniteElements(sub_dm_elastic,
      //                                       dirichlet_bc_ptr.get());
      CHKERR DMoFEMLoopFiniteElements(sub_dm_elastic, "ELASTIC", elastic_fe);
      // Assemble pres
      CHKERR DMoFEMPostProcessFiniteElements(sub_dm_elastic,
                                             dirichlet_bc_ptr.get());
    }

    CHKERR MatDestroy(&K);
    CHKERR VecDestroy(&r_vec);
    CHKERR VecDestroy(&d);
    CHKERR VecDestroy(&commonData.massVec);
    CHKERR VecDestroy(&commonData.changeVec); // this should be in destructor
    CHKERR VecDestroy(&length_vec); 
    CHKERR VecDestroy(&volume_vec); 
    CHKERR DMDestroy(&sub_dm_rho);
    CHKERR DMDestroy(&sub_dm_elastic);
    // main
    CHKERR MatDestroy(&A);
    CHKERR VecDestroy(&x);
    CHKERR VecDestroy(&f);
    CHKERR DMDestroy(&dm);

  }
  CATCH_ERRORS;
  MoFEM::Core::Finalize();
  return 0;
}
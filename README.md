Topology optimization module {#um_topology_optimization_readme}
========================

This is topology optimization module based on [this paper](https://link.springer.com/article/10.1007/s001580050176) implemented in MoFEM. (under developement)

![gif](top_egz1.gif)


Install topology optimization module in your MoFEM build
=========================
Go to your source users modules

~~~~~
cd $MOFEM_INSTALL_DIR/users_modules/users_modules/
git clone https://karol41@bitbucket.org/karol41/mofem_um_topology_optimization.git topology_optimization
cd ..

touch CMakeCache.txt

make -j 2
cd topology_optimization
~~~~~


Check before run
================

* Check where is your mesh file


Model generation
================
To create a model for topology optimisation, simply generate a mesh with loading (units does not matter). 
In the optional mesh blocks called "DENSITY_" the user can define parts of the mesh that will always be preserved (Dirichlet BC).


Example use: small problem
==========================

~~~~~
./topology_op -my_file cantilever1.cub \
 -my_order 2 \
 -optimize 1 -nb_iter 50 \
 -penalty 3 -young 1e3 -poisson 0.1 -move 0.2 \
 -volume_frac 0.35 -lambda 0.35 | tee log
~~~~~

Example use: Large problem with mesh partitioning
==========================

~~~~~
$MOFEM_INSTALL_DIR/users_modules/tools/mofem_part -my_file my_topo_test_mesh.cub  -my_nparts 6 && mpirun -np 6 \
./topology_op -my_file out.h5m \
-stop_crit 1e-2 -nb_iter 40 -my_order 2 -penalty 5 \
-young 1e3 -poisson 0.1 -move 0.2 -volume_frac 0.35 -lambda 0.01  \
-is_partitioned | tee log
~~~~~


Parameters:

~~~~~
-my_file #path to the mesh file 
-my_order #order of approximation
-nb_iter #maximum number of iterations
-young #youngs modulus (optional)
-order_rho #order of approximation on density (optional)
-poisson #poisson ratio
-move # move limit for stability
-volume_frac #constrained volume fraction
-lambda #smoothing coefficient
-stop_crit #stop tolerance when (change) < stop_crit (optional)
-Lmin #minimum bisection Langrange multiplier (optional)
-Lmax #maximum bisection Langrange multiplier (optional)
-my_max_post_proc_ref_level 1 #level of density postprocessing (order)
~~~~~


Produce output files for Paraview:
=========================

~~~~~
for file in out_*h5m; do if [ ! -f ${file%h5m}vtk ]; then mbconvert $file ${file%h5m}vtk; fi ; done
~~~~~


Default parameters are: 
~~~~~
-ksp_atol 1e-10
-ksp_rtol 1e-10
-ksp_type fgmres
-ksp_monitor
-pc_type lu
-pc_factor_mat_solver_package mumps
-optimize 1
-mat_mumps_icntl_14 800
-mat_mumps_icntl_24 1
-mat_mumps_icntl_13 1
-mat_mumps_icntl_14 800
-mat_mumps_icntl_24 1
-mat_mumps_icntl_13 1;
~~~~~
